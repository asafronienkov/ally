### **The required tools for development**
* yarn -> [installation](https://yarnpkg.com/lang/en/docs/install)
* Optional gradle -> [installation](https://gradle.org/install)

### **Start UI development**
```
    gradle build
    cd frontend -> yarn start or gradle startUI 
```
### **Technology stack on the client side**

Single Web page application:

    Angular v2+
    Responsive Web Design with Twitter Bootstrap

### **Technology stack on the server side**
    Gradle configuration for building, testing and running the application
    Spring Security
    Spring MVC REST + Jackson
    Spring Data JPA + Bean Validation
    MySQL Database updates with Liquibase
    H2 for development

### libpng issues

Installing on some versions of OSX may raise errors with a [missing libpng dependency](https://github.com/tcoopman/image-webpack-loader/issues/51#issuecomment-273597313): 
```
Module build failed: Error: dyld: Library not loaded: /usr/local/opt/libpng/lib/libpng16.16.dylib
```
Use apt-get install [package installation](http://askubuntu.com/questions/840257/e-package-libpng12-0-has-no-installation-candidate-ubuntu-16-10-gnome/840268)

This can be remedied by installing the newest version of libpng with [homebrew](http://brew.sh/):

```sh
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"
PATH="$HOME/.linuxbrew/bin:$PATH"
brew install libpng
```


