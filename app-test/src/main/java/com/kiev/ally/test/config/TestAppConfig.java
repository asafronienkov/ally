package com.kiev.ally.test.config;

import com.kiev.ally.config.ServiceConfig;
import com.kiev.ally.util.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ActiveProfiles;

import javax.sql.DataSource;

@Configuration
@ActiveProfiles(Constants.SPRING_PROFILE_TEST)
@PropertySource({"classpath:/profiles/${spring.profiles.active:test}/app.properties"})
@ComponentScan("com.kiev.ally.presentation.facade")
@Import({ServiceConfig.class})
public class TestAppConfig {

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder
            .setType(EmbeddedDatabaseType.H2) //.H2 or .DERBY
            .build();
    }
}
