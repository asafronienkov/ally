package com.kiev.ally.facade;

import com.kiev.ally.domain.Address;
import com.kiev.ally.domain.ImageWrapper;
import com.kiev.ally.exception.RegistrationException;
import com.kiev.ally.presentation.dto.OrganizationDto;
import com.kiev.ally.presentation.facade.OrganizationFacade;
import com.kiev.ally.test.config.TestAppConfig;
import com.kiev.ally.util.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Set;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestAppConfig.class})
@ActiveProfiles(Constants.SPRING_PROFILE_TEST)
public class OrganizationFacadeTest {

    @Autowired
    private OrganizationFacade organizationFacade;

    @Test(expected = RegistrationException.class)
    public void equalsShouldBePersistedOnlyOneTest() throws Exception {
        Address address = new Address("123");
        OrganizationDto sportLife = new OrganizationDto();
        sportLife.setName("Sport Life");
        sportLife.getAddresses().add(address);
        final Set<ImageWrapper> images = Collections.singleton(new ImageWrapper("some", new byte[10]));
        organizationFacade.save(sportLife, images);
        OrganizationDto same = new OrganizationDto();
        same.setName("Sport Life");
        same.getAddresses().add(address);
        organizationFacade.save(same, images);
    }
}
