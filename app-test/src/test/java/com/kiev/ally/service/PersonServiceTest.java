package com.kiev.ally.service;

import com.kiev.ally.domain.Person;
import com.kiev.ally.test.config.TestAppConfig;
import com.kiev.ally.util.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestAppConfig.class})
@ActiveProfiles(Constants.SPRING_PROFILE_TEST)
public class PersonServiceTest {

    @Autowired
    private PersonService personService;

    @Test
    public void entityShouldBeSaved() throws Exception {
        final Person person = new Person();
        person.setLogin("Andrey");
        person.setPassword("Safronienkov");
        person.setActivated(true);
        person.setCreatedBy("some");
        person.setCreatedDate(ZonedDateTime.now());
        personService.save(person);
        assertEquals(1, personService.count());

    }
}
