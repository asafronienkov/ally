package com.kiev.ally.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Embeddable
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"placeId"}, callSuper = false)
public class Address {

    @NotNull
    @Column(unique = true)
    private String placeId;

    @Column
    private BigDecimal latitude;

    @Column
    private BigDecimal longitude;

    public Address(String placeId) {
        this.placeId = placeId;
    }
}
