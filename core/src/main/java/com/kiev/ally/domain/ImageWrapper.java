package com.kiev.ally.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode(of = {"name", "data"}, callSuper = false)
@NoArgsConstructor
public class ImageWrapper implements Serializable {

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false, length = 100000)
    private byte[] data;

    public ImageWrapper(String name, byte[] data) {
        this.name = name;
        this.data = data;
    }
}
