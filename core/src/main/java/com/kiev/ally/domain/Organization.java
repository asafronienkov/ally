package com.kiev.ally.domain;

import com.kiev.ally.util.Constants;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Entity
@Table
@SequenceGenerator(name = Constants.ID_GENERATOR, sequenceName = Constants.ID_GENERATOR)
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"name", "addresses"}, callSuper = false)
public class Organization extends AbstractAuditEntity implements Serializable {

    @Id
    @GeneratedValue(generator = Constants.ID_GENERATOR)
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @Column
    private String url;

    @Column
    @ElementCollection(targetClass = Address.class)
    private Set<Address> addresses = new HashSet<>();

    @Column
    @ElementCollection
    private Set<ImageWrapper> images = new HashSet<>();

    public Optional<String> getUrl() {
        return Optional.ofNullable(url);
    }

}
