package com.kiev.ally.exception;

import lombok.Getter;

public class RegistrationException extends IllegalArgumentException {

    @Getter
    private final String reason;

    public RegistrationException(String reason, String msg) {
        super(msg);
        this.reason = reason;
    }
}
