package com.kiev.ally.util;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class Constants {

    //Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_'.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";

    public static final String ANONYMOUS_USER = "anonymoususer";

    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";

    public static final String SPRING_PROFILE_PROFUCTION = "prod";

    public static final String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";

    public static final String SPRING_PROFILE_HEROKU = "heroku";

    public static final String SPRING_PROFILE_TEST = "test";

    public static final String ID_GENERATOR = "ID_GENERATOR";
}
