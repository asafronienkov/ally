import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ProdConfig } from './app/blocks/config/prod.config';
import { AllyAppModule } from './app/app.module';

ProdConfig();



platformBrowserDynamic().bootstrapModule(AllyAppModule);
