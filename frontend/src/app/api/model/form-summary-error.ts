export class FormSummaryError {
    constructor(public controlName: String,
                public errorMsg: String) {
    }
}
