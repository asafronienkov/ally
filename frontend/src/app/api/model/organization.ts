export class Organization {
    name: String;
    url: String;
    addresses: Array<Address>;
}
export class Address {
    constructor(public placeId: String,
                public latitude: number,
                public longitude: number) {
    }
}
