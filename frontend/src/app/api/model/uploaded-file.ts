export class UploadedFile {
    url: String;
    name: String;
    description?: String;

    constructor(public file: File) {
        this.retrieveUrl(file);
        this.retrieveName(file);
    }

    private retrieveUrl(file) {
        if (file) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                if (event.target) {
                    this.url = event.target.result;
                }
            };

            reader.readAsDataURL(file);
        }
    }

    private retrieveName(file: File) {
        this.name = file.name.split('.')[0];
    }
}
