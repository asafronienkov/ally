import '../vendor.ts';

import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {Ng2Webstorage} from 'ng2-webstorage';

import {HomeComponent} from './home';
import {appRoutes} from './app.route';

import {AppComponent} from './layout';
import {AllySharedModule} from './shared';


@NgModule({
    imports: [
        AllySharedModule.forRoot(),
        RouterModule.forRoot(appRoutes, {
            useHash: true
        }),
        BrowserModule,
    ],
    declarations: [
        AppComponent,
        HomeComponent,
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: Window, useValue: window},
        {provide: Document, useValue: document},
    ]
})
export class AllyAppModule {
}
