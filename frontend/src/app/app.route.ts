import {Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';

export const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'organization', loadChildren: './modules/organization/organization.module#AllyOrganizationModule'}
];
