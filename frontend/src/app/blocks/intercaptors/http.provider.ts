import { Injector } from '@angular/core';
import { Http, XHRBackend, RequestOptions } from '@angular/http';

import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import {EventManager} from "@angular/platform-browser";
import {InterceptableHttp} from "ng-jhipster";

export function interceptableFactory(
    backend: XHRBackend,
    defaultOptions: RequestOptions,
    localStorage: LocalStorageService,
    sessionStorage: SessionStorageService,
    injector: Injector,
    eventManager: EventManager
) {
    return new InterceptableHttp(
        backend,
        defaultOptions,
        [
            // new AuthInterceptor(localStorage, sessionStorage),
            // new AuthExpiredInterceptor(injector),
            // // Other interceptors can be added here
            // new ErrorHandlerInterceptor(eventManager),
            // new NotificationInterceptor()
        ]
    );
};

export function customHttpProvider() {
    return {
        provide: Http,
        useFactory: interceptableFactory,
        deps: [
            XHRBackend,
            RequestOptions,
            LocalStorageService,
            SessionStorageService,
            Injector,
            EventManager
        ]
    };
};
