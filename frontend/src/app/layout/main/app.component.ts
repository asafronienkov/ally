import {Component} from '@angular/core';
@Component({
    selector: 'ally-app',
    templateUrl: './app.component.html',
    styleUrls: [
        'app.scss'
    ]
})
export class AppComponent {
}
