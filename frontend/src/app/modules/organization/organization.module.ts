import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AllySharedModule} from '../../shared';
import {OrganizationRegistrationComponent, routing} from './';

@NgModule({
    imports: [
        routing,
        AllySharedModule
    ],
    declarations: [
        OrganizationRegistrationComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AllyOrganizationModule {
}
