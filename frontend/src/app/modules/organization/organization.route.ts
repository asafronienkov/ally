import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrganizationRegistrationComponent } from './';

const routes: Routes = [
    { path: 'register', component: OrganizationRegistrationComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
