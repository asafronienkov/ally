import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import {OrganizationService} from './organization.service';
import {Subscription} from 'rxjs';
import {COUNTRIES} from '../../../app.constants';

import {FormSummaryError, Organization, Address, Country, UploadedFile} from '../../../api';

@Component({
    selector: 'ally-org-registration',
    templateUrl: 'organization-registration.html',
    providers: [OrganizationService],
    styleUrls: [
        'organization-registration.scss'
    ]

})
export class OrganizationRegistrationComponent implements OnInit, OnDestroy {
    private IMAGE_EXTENSION: string = 'image';
    latitude: number;
    longitude: number;
    zoom: number = 12;
    organization: Organization = new Organization();
    registrationForm: FormGroup;
    formErrors: Array<FormSummaryError>;
    sub: Subscription;
    countries: Array<Country>;
    selectedCountry: Country;
    files: Array<UploadedFile>;

    constructor(private organizationService: OrganizationService,
                private fb: FormBuilder) {
        this.registrationForm = this.fb.group({});
        this.formErrors = [];
        this.countries = COUNTRIES;
    }

    ngOnInit(): void {
        // set current position
        this.setCurrentPosition();
        this.initializeForm();
    }

    private setCurrentPosition() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
            });
        }
    }

    initializeForm() {
        this.registrationForm = this.fb.group({
            name: ['',
                [
                    Validators.required,
                    Validators.minLength(3),
                    Validators.pattern('[a-zA-Z0-9 ]+')
                ]
            ],
            url: [''],
            addresses: this.fb.array([
                this.initAddress()
            ])
        });
    }

    private initAddress() {
        return this.fb.group({
            placeId: ['', [Validators.required]]
        });
    }

    ngOnDestroy(): void {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    changeCountry(selected: Country) {
        this.selectedCountry = new Country(selected.name, selected.code);
    }

    addressChanged(placeResult, idx: number) {
        if (!this.organization.addresses) {
            this.organization.addresses = [];
        }
        // verify result
        if (placeResult.geometry === undefined || placeResult.geometry === null) {
            return;
        }
        // set latitude, longitude and zoom
        this.latitude = placeResult.geometry.location.lat();
        this.longitude = placeResult.geometry.location.lng();
        this.zoom = 18;
        let address = this.findAddressByIndex(idx);
        if (address) {
            address.placeId = placeResult.placeId;
            address.latitude = this.latitude;
            address.longitude = this.longitude;
        } else {
            this.organization.addresses.push(new Address(placeResult.place_id, this.latitude, this.longitude));
        }
    }

    addAddress() {
        const control = <FormArray>this.registrationForm.controls['addresses'];
        if (control.valid && this.findAddressByIndex(control.length - 1)) {
            control.push(this.initAddress());
        }
    }

    showCurrentAddress(idx: number) {
        let address = this.findAddressByIndex(idx);
        if (address) {
            this.longitude = address.longitude;
            this.latitude = address.latitude;
        }
    }

    private findAddressByIndex(idx: number) {
        return this.organization.addresses ? this.organization.addresses.find(function (val, index) {
                return index === idx;
            }) : false;
    }

    removeAddress(index) {
        const control = <FormArray>this.registrationForm.controls['addresses'];
        control.removeAt(index);
        if (this.findAddressByIndex(index)) {
            this.organization.addresses.pop();
        }
    }

    registerOrganization() {
        if (this.registrationForm.valid) {
            this.organization.name = this.registrationForm.value.name;
            this.organization.url = this.registrationForm.value.url;
            this.sub = this.organizationService.save(this.organization, this.files.map(x => x.file))
                .subscribe(x => console.log(x));

        } else {
            this.showErrorSummary();
        }
    }

    showErrorSummary() {
        for (let ctrlName in this.registrationForm.controls) {
            let ctrl = this.registrationForm.controls[ctrlName];
            if (ctrl.errors) {
                this.formErrors.push(new FormSummaryError(ctrlName,
                    JSON.stringify(ctrl.errors)
                ));
            }
        }
    }

    selectedFiles(event) {
        this.files = [];
        let fileList: FileList = event.target.files;
        for (let i = 0; i < fileList.length; i++) {
            let file = fileList[i];
            if (file.type.includes(this.IMAGE_EXTENSION)) {
                this.files.push(new UploadedFile(file));
            }
        }
    }
}

