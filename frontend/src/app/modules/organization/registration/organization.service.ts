import {Inject, Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {Organization} from '../../../api';

@Injectable()
export class OrganizationService {
    constructor(private http: Http) {
    }

    get(): Observable<any> {
        return this.http.get('api/account').map((res: Response) => res.json());
    }

    save(organization: Organization, files: File []): Observable<Response> {
        let data = new FormData();
        data.append('content-type', 'application/json');
        files.forEach(file => {
            data.append('files', file, file.name);
        });
        data.append('organization', new Blob([JSON.stringify(organization)], {
            type: 'application/json'
        }));
        return this.http.post('/api/organization/register', data);
    }

}
