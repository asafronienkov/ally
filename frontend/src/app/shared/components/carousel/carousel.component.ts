import {Component, Input} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {UploadedFile} from '../../../api/model/uploaded-file';

@Component({
    selector: 'ally-carousel',
    templateUrl: './carousel.html',
    styleUrls: [
        'carousel.scss'
    ]
})
export class FileCarouselComponent {
    @Input()
    files: Array<UploadedFile>;

    constructor(config: NgbCarouselConfig) {
        config.interval = 0;
    }
}
