import {Component, Input} from '@angular/core';
import {FormSummaryError} from '../../../api';

@Component({
    selector: 'ally-error-summary',
    templateUrl: 'error.summary.html',
    styleUrls: [
        'error-summary.scss'
    ]
})
export class ErrorSummaryComponent {
    @Input()
    errors: FormSummaryError;
}
