import {
    Component, NgZone, ElementRef, ViewChild, OnInit, EventEmitter, SimpleChanges, OnChanges,
    Input, Output
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MapsAPILoader} from 'angular2-google-maps/core';
import PlaceResult = google.maps.places.PlaceResult;
import {Country} from '../../../api';

@Component({
    moduleId: module.id,
    selector: 'ally-search-address',
    templateUrl: 'search-address.html',
    styleUrls: [
        'search-address.scss'
    ]
})
export class AddressSearchComponent implements OnChanges {
    @Input()
    group: FormGroup;
    @Input()
    country: Country;
    @Output()
    placeResult: EventEmitter<PlaceResult> = new EventEmitter<PlaceResult>();
    autocomplete: google.maps.places.Autocomplete;

    @ViewChild('search')
    public searchElementRef: ElementRef;

    constructor(private mapsAPILoader: MapsAPILoader,
                private ngZone: NgZone) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.country) {
            // load Places Autocomplete
            if (!this.autocomplete) {
                this.mapsAPILoader.load().then(() => {
                    this.autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                        types: ['address'],
                        componentRestrictions: {country: this.country.code.toString()}
                    });
                    this.autocomplete.addListener('place_changed', () => {
                        this.ngZone.run(() => {
                            // get the place result
                            let place: PlaceResult = this.autocomplete.getPlace();
                            this.placeResult.next(place);
                        });
                    });
                });
            } else {
                this.autocomplete.setComponentRestrictions({country: this.country.code.toString()});
            }
        }
    }
}
