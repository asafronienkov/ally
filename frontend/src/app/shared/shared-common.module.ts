import {AllySharedLibsModule} from './shared-libs.module';
import {NgModule} from '@angular/core';
import {ErrorSummaryComponent, FileCarouselComponent, AddressSearchComponent} from './components';

@NgModule({
    imports: [
        AllySharedLibsModule
    ],
    declarations: [
        FileCarouselComponent,
        AddressSearchComponent,
        ErrorSummaryComponent
    ],
    exports: [
        AllySharedLibsModule,
        FileCarouselComponent,
        AddressSearchComponent,
        ErrorSummaryComponent
    ]
})
export class AllySharedCommonModule {
}
