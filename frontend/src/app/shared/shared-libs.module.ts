import {NgModule} from '@angular/core';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {InfiniteScrollModule} from 'angular2-infinite-scroll';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {GOOGLE_API_KEY} from '../app.constants';

@NgModule({
    imports: [
        NgbModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: GOOGLE_API_KEY,
            libraries: ['places']
        }),
        MaterialModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule
    ],
    exports: [
        NgbModule,
        AgmCoreModule,
        CommonModule,
        MaterialModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        InfiniteScrollModule
    ]
})
export class AllySharedLibsModule {
}
