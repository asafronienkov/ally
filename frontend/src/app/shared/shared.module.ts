import {NgModule, ModuleWithProviders} from '@angular/core';
import {AllySharedCommonModule} from './shared-common.module';

@NgModule({
    imports: [
        AllySharedCommonModule
    ],
    exports: [
        AllySharedCommonModule
    ]
})
export class AllySharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AllySharedModule,
            providers: []
        };
    }
}
