package com.kiev.ally.presentation.converter;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

interface AbstractConverter<T, R> extends Function<T, R> {

    default List<R> apply(Collection<T> from) {
        return from.stream().map(this).collect(toList());
    }
}
