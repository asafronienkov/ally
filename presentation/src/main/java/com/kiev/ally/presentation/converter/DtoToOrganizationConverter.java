package com.kiev.ally.presentation.converter;

import com.kiev.ally.domain.Organization;
import com.kiev.ally.presentation.dto.OrganizationDto;

import static org.springframework.beans.BeanUtils.copyProperties;

public class DtoToOrganizationConverter implements AbstractConverter<OrganizationDto, Organization> {


    @Override
    public Organization apply(OrganizationDto source) {
        Organization target = new Organization();
        copyProperties(source, target);
        return target;
    }
}
