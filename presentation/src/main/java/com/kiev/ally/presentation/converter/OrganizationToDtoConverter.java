package com.kiev.ally.presentation.converter;

import com.kiev.ally.domain.Organization;
import com.kiev.ally.presentation.dto.OrganizationDto;

import java.util.function.Function;

import static org.springframework.beans.BeanUtils.copyProperties;

public class OrganizationToDtoConverter implements Function<Organization, OrganizationDto> {

    @Override
    public OrganizationDto apply(Organization source) {
        OrganizationDto target = new OrganizationDto();
        copyProperties(source, target);
        return target;
    }
}
