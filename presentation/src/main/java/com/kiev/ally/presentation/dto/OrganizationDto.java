package com.kiev.ally.presentation.dto;

import com.kiev.ally.domain.Address;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class OrganizationDto {

    private long id;

    private String name;

    private String url;

    private Set<Address> addresses = new HashSet<>();

    private Set<File> images = new HashSet<>();
}
