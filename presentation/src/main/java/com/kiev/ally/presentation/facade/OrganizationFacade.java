package com.kiev.ally.presentation.facade;

import com.kiev.ally.domain.ImageWrapper;
import com.kiev.ally.presentation.dto.OrganizationDto;

import java.util.Set;

public interface OrganizationFacade {

    OrganizationDto save(OrganizationDto organization, Set<ImageWrapper> files);

    long count();
}
