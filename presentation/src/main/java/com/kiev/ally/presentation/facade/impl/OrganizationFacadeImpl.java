package com.kiev.ally.presentation.facade.impl;

import com.kiev.ally.domain.ImageWrapper;
import com.kiev.ally.domain.Organization;
import com.kiev.ally.exception.RegistrationException;
import com.kiev.ally.presentation.converter.DtoToOrganizationConverter;
import com.kiev.ally.presentation.converter.OrganizationToDtoConverter;
import com.kiev.ally.presentation.dto.OrganizationDto;
import com.kiev.ally.presentation.facade.OrganizationFacade;
import com.kiev.ally.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OrganizationFacadeImpl implements OrganizationFacade {

    private final OrganizationService organizationService;

    private final DtoToOrganizationConverter toOrganizationConverter;

    private final OrganizationToDtoConverter toDtoOrganizationConverter;

    @Autowired
    public OrganizationFacadeImpl(OrganizationService organizationService) {
        this.organizationService = organizationService;
        toOrganizationConverter = new DtoToOrganizationConverter();
        toDtoOrganizationConverter = new OrganizationToDtoConverter();
    }

    @Override
    public OrganizationDto save(OrganizationDto organizationDto, Set<ImageWrapper> files) throws RegistrationException {
        if (organizationService.findByName(organizationDto.getName()).isPresent()) {
            throw new RegistrationException("Name duplicate", "Organization name already exists");
        }
        final Organization organization = toOrganizationConverter.apply(organizationDto);
        organization.setImages(files);
        final Organization registered = organizationService.save(organization);

        return toDtoOrganizationConverter.apply(registered);
    }

    @Override
    public long count() {
        return organizationService.count();
    }
}
