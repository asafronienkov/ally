package com.kiev.ally.config;

import org.hibernate.cfg.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.kiev.ally.repository")
public class DatabaseConfig {

    @Value("${hibernate.dialect}")
    private String dialect;

    @Value("${hibernate.show_sql}")
    private boolean showSql;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hbm2ddlAuto;

    @Value("${hibernate.batch.size}")
    private int batchSize;

    @Value("${hibernate.cache.region.factory_class}")
    private String cacheRegionFactory;

    @Value("${hibernate.cache.use_second_level_cache}")
    private boolean useSecondLevelCache;

    @Value("${hibernate.cache.use_query_cache}")
    private boolean useQueryCache;

    @Bean
    @Profile("!test")
    public DataSource dataSource(@Value("${datasource.className}") final String className,
                                 @Value("${datasource.url}") final String url,
                                 @Value("${datasource.username}") final String username,
                                 @Value("${datasource.password}") final String pass) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(className);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(pass);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPackagesToScan("com.kiev.ally.domain");

        entityManagerFactoryBean.setJpaProperties(getHibernateProperties());

        return entityManagerFactoryBean;
    }
    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put(AvailableSettings.DIALECT, dialect);
        properties.put(AvailableSettings.SHOW_SQL, showSql);
        properties.put(AvailableSettings.STATEMENT_BATCH_SIZE, batchSize);
        properties.put(AvailableSettings.HBM2DDL_AUTO, hbm2ddlAuto);
        properties.put(AvailableSettings.USE_SECOND_LEVEL_CACHE, useSecondLevelCache);
        properties.put(AvailableSettings.CACHE_REGION_FACTORY, cacheRegionFactory);
        properties.put(AvailableSettings.CACHE_PROVIDER_CONFIG, "ehcache.xml");
        properties.put(AvailableSettings.USE_QUERY_CACHE, useQueryCache);
        return properties;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

}
