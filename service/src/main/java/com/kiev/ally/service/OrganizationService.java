package com.kiev.ally.service;

import com.kiev.ally.domain.Organization;

import java.util.Optional;

public interface OrganizationService {

    Organization save(Organization organization);

    long count();

    Optional<Organization> findByName(String name);
}
