package com.kiev.ally.service;

import com.kiev.ally.domain.Person;

public interface PersonService {

    Person save(Person person);

    long count();
}
