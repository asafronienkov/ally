package com.kiev.ally.service.impl;

import com.kiev.ally.domain.Organization;
import com.kiev.ally.repository.OrganizationRepository;
import com.kiev.ally.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;

    @Autowired
    public OrganizationServiceImpl(final OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Override
    public Organization save(Organization organization) {
        return organizationRepository.save(organization);
    }

    @Override
    @Transactional(readOnly = true)
    public long count() {
        return organizationRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Organization> findByName(String name) {
        return organizationRepository.findByName(name);
    }
}
