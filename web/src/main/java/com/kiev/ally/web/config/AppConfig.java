package com.kiev.ally.web.config;

import com.kiev.ally.config.ServiceConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource({"classpath:/profiles/${spring.profiles.active:dev}/app.properties"})
@ComponentScan({"com.kiev.ally.presentation.facade"})
@Import({ServiceConfig.class})
public class AppConfig {

}
