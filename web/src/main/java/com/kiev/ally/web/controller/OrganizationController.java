package com.kiev.ally.web.controller;

import com.kiev.ally.domain.ImageWrapper;
import com.kiev.ally.exception.RegistrationException;
import com.kiev.ally.presentation.dto.OrganizationDto;
import com.kiev.ally.presentation.facade.OrganizationFacade;
import com.kiev.ally.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping("/api/organization")
public class OrganizationController {

    private static final String ENTITY_NAME = "Organization";

    private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationController.class);

    private final OrganizationFacade organizationFacade;

    @Autowired
    public OrganizationController(OrganizationFacade organizationFacade) {
        this.organizationFacade = organizationFacade;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestPart OrganizationDto organization, @RequestPart MultipartFile[] files) throws URISyntaxException {
        try {
            final OrganizationDto registered = organizationFacade.save(organization, convertToImage(files));
            return ResponseEntity.created(new URI("/api/organization/registration/" + organization.getId()))
                .headers(HeaderUtil.createAlert("A organization registered", organization.getName()))
                .body(registered);
        } catch (RegistrationException ex) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, ex.getReason(), ex.getMessage()))
                .body(null);
        }
    }

    private Set<ImageWrapper> convertToImage(MultipartFile... files) {
        return Arrays.stream(files)
            .peek(file -> {
                if (!file.getContentType().contains("image")) {
                    throw new RegistrationException("Wrong file type", "File content is not images");
                }
            })
            .map(originFile -> {
                try {
                    return new ImageWrapper(originFile.getOriginalFilename(), originFile.getBytes());
                } catch (IOException e) {
                    LOGGER.error("Can not write file {}", e);
                    throw new IllegalArgumentException(e);
                }
            })
            .collect(toSet());
    }

}
